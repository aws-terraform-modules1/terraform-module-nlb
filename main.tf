resource "aws_lb" "odai_NLB" {
  name                       = var.NLB_Name
  load_balancer_type         = "network"
  enable_deletion_protection = false

  subnets = [module.NLB_VPC.odai_subnet_id]

  tags = {
    Environment = var.NLB_Env
  }
}

resource "aws_internet_gateway" "big_door" {
  vpc_id = module.NLB_VPC.VPC_ID
  tags = {
    Name = var.Internet_Gateway_Name
  }
}

module "NLB_VPC" {
  source = "git::https://gitlab.com/aws-terraform-modules1/terraform-module-vpc.git"
  cidr_block = var.vpc_cidr_block
  subnet_cidr_block = var.subnet_cidr_block
  availability_zone = var.vpc_az
  Name = var.vpc_name
}