variable "NLB_Name" {
  type        = string
  description = "(string, Required) Whatever you want to call your AWS Network LoadBalancer, only alphanumeric"
  default     = "balancerofloadsnetwork"
}

#variable "NLB_Subnets" {
#  type        = set(string)
#  description = "(set(string), Optional) The ID of the subnet your instances will be in. (aws_subnet.example.id)"
#}

variable "NLB_Env" {
  type        = string
  description = "(string, Optional) Either Production or Development, whatever stage you're in."
}

variable "Internet_Gateway_Name" {
  type = string
  description = "(string, Required) Whatever you want to call your internet gateway on AWS"
}

variable "vpc_cidr_block" {
    type = string
    description = "(string, Required)"
}

variable "subnet_cidr_block" {
  type = string
  description = "(string, Required)"
}

variable "vpc_az" {
  type = string
  description = "(string, Required)"
}

variable "vpc_name" {
  type = string
  description = "(string, Required) Whatever you want to call your VPC"
}